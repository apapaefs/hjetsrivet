#!/usr/bin/env python

import yoda
import numpy as np
import matplotlib as mpl
import pandas

def read_yoda(path, scaleY=1):
  "reads in a yoda file from 'path'"
  aos = yoda.read(path, asdict=True)
  for key, ao in aos.items():
    if(ao.type() != "Scatter2D"):
      aos[key] = yoda.core.mkScatter(ao)
    if(aos[key].type() == "Scatter2D"):
      aos[key].scaleY(scaleY)
    #print(aos[key].annotationsDict)
    annotationsDict = aos[key].annotationsDict()
    for n, v in annotationsDict.items():
      aos[key].setAnnotation(n.lower(),v)
    if scaleY != 1:
      if aos[key].hasAnnotation("underflow"):
        aos[key].setAnnotation("underflow", scaleY*float(aos[key].annotation("underflow")))
      if aos[key].hasAnnotation("underflow_min"):
        aos[key].setAnnotation("underflow_min", scaleY*float(aos[key].annotation("underflow_min")))
      if aos[key].hasAnnotation("underflow_max"):
        aos[key].setAnnotation("underflow_max", scaleY*float(aos[key].annotation("underflow_max")))
      if aos[key].hasAnnotation("overflow"):
        aos[key].setAnnotation("overflow", scaleY*float(aos[key].annotation("overflow")))
      if aos[key].hasAnnotation("overflow_min"):
        aos[key].setAnnotation("overflow_min", scaleY*float(aos[key].annotation("overflow_min")))
      if aos[key].hasAnnotation("overflow_max"):
        aos[key].setAnnotation("overflow_max", scaleY*float(aos[key].annotation("overflow_max")))
  return aos


def print_xs(name, pnt):
  print('{0:<20} ${1:.4f}^{{+{2:.4f}}}_{{-{3:.4f}}}$'.format(name,
    pnt.y(), pnt.yMax()-pnt.y(), pnt.y()-pnt.yMin()))
  return pnt.y

def xs_from_integral(name, histo):
  vals = np.array(histo.yVals())
  vals_min = np.array(histo.yMins())
  vals_max = np.array(histo.yMaxs())
  widths = np.array(histo.xMaxs())-np.array(histo.xMins())
  xs = sum(vals*widths)
  if histo.hasAnnotation("underflow"):
    xs += float(histo.annotation("underflow"))
  if histo.hasAnnotation("overflow"):
    xs += float(histo.annotation("overflow"))
  xs_min = sum(vals_min*widths)
  if histo.hasAnnotation("underflow_min"):
    xs_min += float(histo.annotation("underflow_min"))
  if histo.hasAnnotation("overflow_min"):
    xs_min += float(histo.annotation("overflow_min"))
  xs_max = sum(vals_max*widths)
  if histo.hasAnnotation("underflow_max"):
    xs_min += float(histo.annotation("underflow_max"))
  if histo.hasAnnotation("overflow_max"):
    xs_min += float(histo.annotation("overflow_max"))
  print('{0:<20} ${1:.4f}^{{+{2:.4f}}}_{{-{3:.4f}}}$'.format(name,
    xs, xs_max-xs, xs-xs_min))

def setup_axes(hs, main_axis, ratio_axis, **plotkeys):
  import matplotlib.pyplot as plt
  from   matplotlib.ticker import AutoMinorLocator, LogLocator, MaxNLocator

  ## Plot range limits
  xmin = float(plotkeys.get("xmin", min(h.xMin() for h in hs)))
  xmax = float(plotkeys.get("xmax", max(h.xMax() for h in hs)))
  ymin = float(plotkeys.get("ymin", 0.9*min(min(h.yVals()) for h in hs)))
  ymax = float(plotkeys.get("ymax", 1.1*max(max(h.yVals()) for h in hs)))
  main_axis.set_xlim([xmin,xmax])
  main_axis.set_ylim([ymin,ymax])
  #
  if ratio_axis:
    ymin_ratio = float(plotkeys.get("ratioymin", 0.6))
    ymax_ratio = float(plotkeys.get("ratioymax", 1.4))
    for ax in ratio_axis:
      ax.set_xlim([xmin,xmax])
      ax.set_ylim([ymin_ratio,ymax_ratio])

  main_axis.tick_params(which='major', labelsize=8, length=8)
  main_axis.tick_params(which='minor', length=4)
  main_axis.tick_params(which='both', right=True, top=True, direction='in')
  if ratio_axis:
    plt.setp(main_axis.get_xticklabels(), visible=False)
    for ax in ratio_axis:
      #print('ax=',ax)
      ax.tick_params(which='major', labelsize=8, length=8)
      ax.tick_params(which='minor', length=4)
      ax.tick_params(which='both', right=True, top=True, direction='in')
      plt.setp(ax.get_xticklabels(), visible=True)
    plt.setp(ratio_axis[-1].get_xticklabels(), visible=True)
    # ratio_axis.grid(True, which='major',linestyle='dotted')
  # main_axis.grid(True, which='major',linestyle='dotted')

  xmeasure      = "log" if plotkeys.get("logx", False) else "linear"
  ymeasure      = "log" if plotkeys.get("logy", False) else "linear"
  ratioymeasure = "log" if plotkeys.get("ratiology", False) else "linear"
  main_axis.set_xscale(xmeasure)
  main_axis.set_yscale(ymeasure)
  if ratio_axis:
    for ax in ratio_axis:
      ax.set_xscale(xmeasure)
      ax.set_yscale(ratioymeasure)
      if plotkeys.get("ratiology", False):
        ax.yaxis.set_major_locator(LogLocator())
      else:
        ax.yaxis.set_major_locator(MaxNLocator(5,prune="upper", steps=[1,2,5,10]))
        ax.yaxis.set_minor_locator(AutoMinorLocator())

  if plotkeys.get("logx", False):
    main_axis.xaxis.set_major_locator(LogLocator())
    if ratio_axis:
      for ax in ratio_axis:
        ax.xaxis.set_major_locator(LogLocator())
  else:
    main_axis.xaxis.set_minor_locator(AutoMinorLocator())
    if ratio_axis:
      for ax in ratio_axis:
        ax.xaxis.set_minor_locator(AutoMinorLocator())

  if plotkeys.get("logy", False):
    main_axis.yaxis.set_major_locator(LogLocator())
  else:
    main_axis.yaxis.set_minor_locator(AutoMinorLocator())



def set_axis_labels(axmain, axratio=None, xlabel=None, ylabel=None, ratioylabel=None, **kwargs):
    axmain.set_ylabel(ylabel, y=1, ha="right", labelpad=None, **kwargs)
    if axratio:
      for i in range(len(axratio)):
        axratio[i].set_ylabel(ratioylabel[i],**kwargs)
      axratio[-1].set_xlabel(xlabel, x=1, ha="right", labelpad=None, **kwargs)
    else:
        axmain.set_xlabel(xlabel, x=1, ha="right", labelpad=None, **kwargs)

def init_fig(hs, **plotkeys):
  "initialises the figures and axes for plotting"

  import yoda.plotting as yplt
  import matplotlib.pyplot as plt

  #mpl.rcParams['text.usetex']         = True
  #mpl.rcParams['text.latex.unicode']  = True
  #mpl.rcParams['text.latex.preamble'] = ['\\usepackage{amsmath}']
  # mpl.rcParams['font.size']           = 12

  ratio = plotkeys.get("ratio", False)
  title = plotkeys.get("title", None)
  figsize = plotkeys.get("figsize", (8,6))
  height_ratios = plotkeys.get("height_ratios", [2,1])
  fig = plt.figure(figsize=figsize, dpi=600)
  if title:
    fig.suptitle(title, horizontalalignment="left", x=0.13)
  if ratio:
    num_subplots = len(height_ratios)
    gs = mpl.gridspec.GridSpec(num_subplots, 1, height_ratios=height_ratios, hspace=0)
    main_axis = fig.add_subplot(gs[0])
    ratio_axis = []
    for i in range(1,num_subplots):
      ratio_axis.append(fig.add_subplot(gs[i], sharex=main_axis))
      ratio_axis[-1].axhline(1.0, color="gray")
  else:
    main_axis = fig.add_subplot(1,1,1)
    ratio_axis = None

  xlabel=plotkeys.get("xlabel", "TODO")
  ylabel=plotkeys.get("ylabel", "TODO")
  if ratio:
    ratioylabel=plotkeys.get("ratioylabel", "TODO")
  else:
    ratioylabel=None

  set_axis_labels(main_axis,ratio_axis,xlabel=xlabel,ylabel=ylabel,
    ratioylabel=ratioylabel,size=10)
  return fig, (main_axis, ratio_axis)

def _devide_ignore_0(a,b):
  return np.divide(a, b, out=np.zeros_like(a), where=b!=0)

def plot_histo(axmain, axratio, h, href=None, color="black", errorbars=True, **plotkeys):
  "plot a histogram"

  lcolor = plotkeys.setdefault("color", color)
  lstyle = plotkeys.pop("linestyle", "-")
  lwidth = plotkeys.pop("linewidth", 1.8)

  bins = np.append(h.xMins(), h.xMax())
  yvals = np.append(h.yVals(), h.yVals()[-1])

#  yvals = np.array(yvals, dtype=float)
  if errorbars:
    ymaxs = np.append(h.yMaxs(), h.yMaxs()[-1])
    ymins = np.append(h.yMins(), h.yMins()[-1])
#    ymins = np.array(ymins, dtype=float)
#    ymaxs = np.array(ymaxs, dtype=float)
    if axmain:
      axmain.fill_between(bins, ymins, ymaxs, step="post", alpha=0.5, linewidth=0, **plotkeys) 
  if axmain:
    rtrn_line = axmain.step(bins, yvals,
      where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  else:
    rtrn_line = None
  rtrn_line_ratio = None
  if href and axratio:  ## ratio plot
      yvals_ref = np.append(href.yVals(), href.yVals()[-1])
      yratios = _devide_ignore_0(yvals, yvals_ref)
      if errorbars:
        ymax_ratios = _devide_ignore_0(ymaxs, yvals_ref)
        ymin_ratios = _devide_ignore_0(ymins, yvals_ref)
        axratio.fill_between(bins, ymin_ratios, ymax_ratios,
          step="post", alpha=0.5, linewidth=0, **plotkeys)
      rtrn_line_ratio = axratio.step(bins, yratios,
        where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  return rtrn_line, rtrn_line_ratio

def plot_histo_nocentral(axmain, axratio, h, href=None, color="black", errorbars=True, **plotkeys):
  "plot a histogram"

  lcolor = plotkeys.setdefault("color", color)
  lstyle = plotkeys.pop("linestyle", "-")
  lwidth = plotkeys.pop("linewidth", 1.8)

  bins = np.append(h.xMins(), h.xMax())
  yvals = np.append(h.yVals(), h.yVals()[-1])

#  yvals = np.array(yvals, dtype=float)
  if errorbars:
    ymaxs = np.append(h.yMaxs(), h.yMaxs()[-1])
    ymins = np.append(h.yMins(), h.yMins()[-1])
#    ymins = np.array(ymins, dtype=float)
#    ymaxs = np.array(ymaxs, dtype=float)
    if axmain:
      axmain.fill_between(bins, ymins, ymaxs, step="post", alpha=0.5, linewidth=0, **plotkeys) 
  #if axmain:
    #rtrn_line = axmain.step(bins, yvals,
      #where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  #else:
  #  rtrn_line = None
  rtrn_line_ratio = None
  if href and axratio:  ## ratio plot
      yvals_ref = np.append(href.yVals(), href.yVals()[-1])
      yratios = _devide_ignore_0(yvals, yvals_ref)
      if errorbars:
        ymax_ratios = _devide_ignore_0(ymaxs, yvals_ref)
        ymin_ratios = _devide_ignore_0(ymins, yvals_ref)
        axratio.fill_between(bins, ymin_ratios, ymax_ratios,
          step="post", alpha=0.5, linewidth=0, **plotkeys)
      #rtrn_line_ratio = axratio.step(bins, yratios,
      #  where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  return 1

def plot_histo_givencentral(axmain, axratio, h, hcentral, href=None, color="black", errorbars=True, **plotkeys):
  "plot a histogram"

  lcolor = plotkeys.setdefault("color", color)
  lstyle = plotkeys.pop("linestyle", "-")
  lwidth = plotkeys.pop("linewidth", 1.8)

  bins = np.append(h.xMins(), h.xMax())
  yvals = np.append(hcentral.yVals(), hcentral.yVals()[-1])

#  yvals = np.array(yvals, dtype=float)
  if errorbars:
    ymaxs = np.append(h.yMaxs(), h.yMaxs()[-1])
    ymins = np.append(h.yMins(), h.yMins()[-1])
#    ymins = np.array(ymins, dtype=float)
#    ymaxs = np.array(ymaxs, dtype=float)
    if axmain:
      axmain.fill_between(bins, ymins, ymaxs, step="post", alpha=0.5, linewidth=0, **plotkeys) 
  if axmain:
    rtrn_line = axmain.step(bins, yvals,
      where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  #else:
  #  rtrn_line = None
  rtrn_line_ratio = None
  if href and axratio:  ## ratio plot
      yvals_ref = np.append(href.yVals(), href.yVals()[-1])
      yratios = _devide_ignore_0(yvals, yvals_ref)
      if errorbars:
        ymax_ratios = _devide_ignore_0(ymaxs, yvals_ref)
        ymin_ratios = _devide_ignore_0(ymins, yvals_ref)
        axratio.fill_between(bins, ymin_ratios, ymax_ratios,
          step="post", alpha=0.5, linewidth=0, **plotkeys)
      rtrn_line_ratio = axratio.step(bins, yratios,
        where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  return 1

def plot_cumulative_histo_givencentral(axmain, axratio, h, hcentral, href=None, color="black", errorbars=True, **plotkeys):
  "plot a cumulative histogram"

  lcolor = plotkeys.setdefault("color", color)
  lstyle = plotkeys.pop("linestyle", "-")
  lwidth = plotkeys.pop("linewidth", 1.8)

  bins = np.append(h.xMins(), h.xMax())
  bins = bins[:-1]
  binwidth = (h.xMax() - h.xMin())/len(bins)
  #print(binwidth)
  #yvals = np.append(hcentral.yVals(), hcentral.yVals()[-1])
  yvals = []
  for i in range(len(hcentral.yVals())):
    yvals.append(binwidth*np.sum(hcentral.yVals()[list(range(i))]))
  yvals = np.array(yvals)  
  #print(yvals, bins)
#  yvals = np.array(yvals, dtype=float)
  if errorbars:
    #ymaxs = np.append(h.yMaxs(), h.yMaxs()[-1])
    #ymins = np.append(h.yMins(), h.yMins()[-1])
    ymins = []
    ymaxs = []
    for i in range(len(h.yMins())):
      ymins.append(binwidth*ymins, binwidth*np.sum(h.yMins()[list(range(i))]))
    for i in range(len(h.yMaxs())):
      ymaxs.append(binwidth*ymaxs, binwidth*np.sum(h.yMaxs()[list(range(i))]))
    ymins = np.array(ymins)
    ymaxs = np.array(ymaxs)
#    ymins = np.array(ymins, dtype=float)
#    ymaxs = np.array(ymaxs, dtype=float)
    if axmain:
      axmain.fill_between(bins, ymins, ymaxs, step="post", alpha=0.5, linewidth=0, **plotkeys) 
  if axmain:
    rtrn_line = axmain.step(bins, yvals, where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  #else:
  #  rtrn_line = None
  rtrn_line_ratio = None
  if href and axratio:  ## ratio plot
      #yvals_ref = np.append(href.yVals(), href.yVals()[-1])
      yvals_ref = []
      for i in range(len(href.yVals())):
          yvals_ref.append(binwidth*np.sum(href.yVals()[list(range(i))]))
      yvals_ref = np.array(yvals_ref)  
      yratios = _devide_ignore_0(yvals, yvals_ref)
      if errorbars:
        ymax_ratios = _devide_ignore_0(ymaxs, yvals_ref)
        ymin_ratios = _devide_ignore_0(ymins, yvals_ref)
        axratio.fill_between(bins, ymin_ratios, ymax_ratios,
          step="post", alpha=0.5, linewidth=0, **plotkeys)
      rtrn_line_ratio = axratio.step(bins, yratios,
        where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  return 1


def plot_xsec_histo_givencentral(axmain, axratio, h, hcentral, href=None, color="black", errorbars=False, **plotkeys):
  "plot a histogram"

  lcolor = plotkeys.setdefault("color", color)
  lstyle = plotkeys.pop("linestyle", "-")
  lwidth = plotkeys.pop("linewidth", 1.8)

  bins = np.append(href.yVals(), href.yVals()[-1])
  yvals = np.append(hcentral.yVals(), hcentral.yVals()[-1])

  #print(bins)
  #print(yvals)
#  yvals = np.array(yvals, dtype=float)
  if errorbars:
    ymaxs = np.append(h.yMaxs(), h.yMaxs()[-1])
    ymins = np.append(h.yMins(), h.yMins()[-1])
#    ymins = np.array(ymins, dtype=float)
#    ymaxs = np.array(ymaxs, dtype=float)
    if axmain:
      axmain.fill_between(bins, ymins, ymaxs, step="post", alpha=0.2, linewidth=0, **plotkeys) 
  if axmain:
    rtrn_line = axmain.step(bins, yvals,
      where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  #else:
  #  rtrn_line = None
  rtrn_line_ratio = None
  if href and axratio:  ## ratio plot
      yvals_ref = np.append(href.yVals(), href.yVals()[-1])
      yratios = _devide_ignore_0(yvals, yvals_ref)
      if errorbars:
        ymax_ratios = _devide_ignore_0(ymaxs, yvals_ref)
        ymin_ratios = _devide_ignore_0(ymins, yvals_ref)
        axratio.fill_between(bins, ymin_ratios, ymax_ratios,step="post", alpha=0.2, linewidth=0, **plotkeys)
      rtrn_line_ratio = axratio.step(bins, yratios,where="post", linewidth=lwidth, linestyle=lstyle, **plotkeys)
  return 1


def add_text(axis, xplace, yplace, text):
  tot_text = text[0]
  for i in range(1, len(text)):
    tot_text += '\n' + text[i]
  axis.text(xplace,yplace,tot_text,
    verticalalignment='center', horizontalalignment='left',
    transform=axis.transAxes, fontsize=10, linespacing=1.3, zorder=0)

def mkPntMinMax(x, y, xmin, xmax, ymin, ymax):
  pnt = yoda.core.Point2D(x,y)
  pnt.setErrMinus(1, x-xmin)
  pnt.setErrPlus( 1, xmax-x)
  pnt.setErrMinus(2, y-ymin)
  pnt.setErrPlus( 2, ymax-y)
  return pnt

def mkPntPlusMinus(x, y, xmin, xmax, yerr_min, yerr_max):
  pnt = yoda.core.Point2D(x,y)
  pnt.setErrMinus(1, x-xmin)
  pnt.setErrPlus( 1, xmax-x)
  pnt.setErrMinus(2, yerr_min)
  pnt.setErrPlus( 2, yerr_max)
  return pnt

def rebin(scat, rebin, ErrorType="env"):
  "return a scatter with #bins/rebin points. Error is treated according to 'ErrorType' (stat or env)"
  from math import sqrt
  import sys
  newscat = yoda.core.Scatter2D(path=scat.path(),title=scat.title())
  for n, v in scat.annotationsDict().items(): # would be easier if scat.reset would work ...
    newscat.setAnnotation(n,v)
  pnts = scat.points()
  for i in range(0, len(pnts), rebin):
      width = pnts[i].xMax() - pnts[i].xMin()
      x = pnts[i].x()
      xmin = pnts[i].xMin()
      xmax = pnts[i].xMax()
      y = pnts[i].y() * width
      if ErrorType == "env":
        ymin = pnts[i].yMin() * width
        ymax = pnts[i].yMax() * width
      elif ErrorType == "stat":
        yerr_min = (pnts[i].errMinus(2) * width)**2
        yerr_max = (pnts[i].errPlus(2)  * width)**2
      else:
        print("Rebinning for ErrorType not implemented.")
        sys.exit(1)
      nbin = 1
      for j in range(1 ,rebin):
        if i+j >= len(pnts):
          break
        nbin += 1
        width = pnts[i+j].xMax() - pnts[i+j].xMin()
        x += pnts[i+j].x()
        xmin = min(xmin, pnts[i+j].xMin())
        xmax = max(xmax, pnts[i+j].xMax())
        y += pnts[i+j].y() * width
        if ErrorType == "env":
          ymin += pnts[i+j].yMin() * width
          ymax += pnts[i+j].yMax() * width
        else:
          yerr_min += (pnts[i].errMinus(2) * width)**2
          yerr_max += (pnts[i].errPlus(2)  * width)**2
      x /= nbin
      y /= xmax-xmin
      if ErrorType == "env":
        ymin /= xmax-xmin
        ymax /= xmax-xmin
        newscat.addPoint(mkPntMinMax(x, y, xmin, xmax, ymin, ymax))
      else:
        yerr_min = sqrt(yerr_min) / (xmax-xmin)
        yerr_max = sqrt(yerr_max) / (xmax-xmin)
        newscat.addPoint(mkPntPlusMinus(x, y, xmin, xmax, yerr_min, yerr_max))
  return newscat


# function to generate html file. 
def write_html(outputdir, htmlfiletag, htmlfiles, title):
    print('Writing out html file:', htmlfiletag + '.html')
    htmlout = open(htmlfiletag + '.html', "w")
    htmlout.write('<!DOCTYPE html>\n')
    htmlout.write('<html>\n')
    htmlout.write('<body>\n')
    htmlout.write('<h1>' + title)
    htmlout.write('<br>\n')
    #htmlout.write('<p> reweighing &beta; = ' + str(angu) + ':</p>')
    for hf in htmlfiles:
        htmlout.write('<div style="width: 33%; float: left">')
        htmlout.write('<font size="1">' + hf + '</font>')
        htmlout.write('<a href="./' + hf +'.pdf">\n')
        htmlout.write('<img src="' + hf + '.png" alt="' + hf + '" style="width:360px;height:240px;border:0;">\n')
        htmlout.write('</a>')
        htmlout.write('</div>')
    htmlout.write('<br>\n')
    htmlout.write('</body>')
    htmlout.write('</html>')
    htmlout.close()
