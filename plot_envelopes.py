#!/usr/bin/env python

import matplotlib as mpl
mpl.use('pdf')
import plotter_helper as myplt
import matplotlib.pyplot as plt
import pandas
import numpy as np
import glob
from plotter_helper import *

## global options
info_text = ['$pp \\to (h\\to \\gamma\\gamma)jj$',
  '$\\mathrm{LHC}@13~\\mathrm{TeV}$',
  '$\\mathrm{anti-kt}, R=0.4, p_{j,\\perp}>30~\\mathrm{GeV},|y_j|<4.4$']
info_text_VBF = ['$|y_{j_1}-y_{j_2}|>2.8, m_{j_1 j_2}>400~\\mathrm{GeV}$']

result_dir="./"
output_dir="plot_output_060621/"

## data
yodas = []

#yodas.append(myplt.read_yoda(result_dir+
#  "h2345j-matched-1/h2345j-matched-1-envelope.yoda", scaleY=2.36))
#yodas.append(myplt.read_yoda(result_dir+"h2345j-matched-rescaled-3/h2345j-matched-rescaled-3-envelope.yoda", scaleY=2.36))
yodas.append(myplt.read_yoda(result_dir+"h2345j-matched-rescaled-4-modhj/h2345j-matched-rescaled-4-modhj-envelope.yoda", scaleY=2.36))
yodas.append(myplt.read_yoda(result_dir+"h2345j-matched-rescaled-6-modhj/h2345j-matched-rescaled-6-modhj-envelope.yoda", scaleY=2.36))
yodas.append(myplt.read_yoda(result_dir+"h2j_NLO_FO-3/h2j_NLO_FO-envelope.yoda", scaleY=2.36))
yodas.append(myplt.read_yoda(result_dir+"vbf_NLO-3/vbfh_NLO-envelope.yoda", scaleY=2.36))

## central data
yodas_central = []
#yodas_central.append(myplt.read_yoda(result_dir+
#  "h2345j-matched-1/allprocs.Scale_MaxMjjMh_MuR1_MuF1.yoda", scaleY=2.36))
#yodas_central.append(myplt.read_yoda(result_dir+"h2345j-matched-rescaled-3/allprocs-rescaled.Scale_MaxMjjMh_MuR1_MuF1.RESCALED.yoda", scaleY=2.36))
yodas_central.append(myplt.read_yoda(result_dir+"h2345j-matched-rescaled-4-modhj/allprocs-rescaled.Scale_MaxMjjMh_MuR1_MuF1.RESCALED.yoda", scaleY=2.36))
yodas_central.append(myplt.read_yoda(result_dir+"h2345j-matched-rescaled-6-modhj/allprocs-rescaled.Scale_MaxMjjMh_MuR1_MuF1.RESCALED.yoda", scaleY=2.36))
yodas_central.append(myplt.read_yoda(result_dir+"h2j_NLO_FO-3/ANALYSIS-820.MUR1_MUF1_PDF13100.yoda", scaleY=2.36))
yodas_central.append(myplt.read_yoda(result_dir+"vbf_NLO-3/ANALYSIS-816.MUR1_MUF1_PDF13100.yoda", scaleY=2.36))

## settings for plot
#names = ["VBF", "HEJRES", "NLO"]
#names = ["HEJ", "HEJRES", "NLO"]
#names = ["HEJRES", "NLO"]
names = ["HEJRES", "HEJRESNEW", "NLO", "VBF"]
#names = ["HEJRES", "VBF"]

lHEJ = '\\mathrm{HEJ}'
names_long = [
'HEJ matched resc. h+j',
'NEWER HEJ matched resc. h+j',
'NLO HEFT h+2j',
'NLO VBF'
]
    
hist_options = [{"color":"black", "linestyle":"-", "errorbars":True},
                {"color":"indigo", "linestyle":":", "errorbars":True},  {"color":"red", "linestyle":":", "errorbars":True}, {"color":"blue", "linestyle":"--", "errorbars":True}]

index_ref = 3
ratiolabel = ['ratio to '+names_long[index_ref]]## xs
xsec_vbf = [0 for j in range(len(names_long))]
print("xs inclusive [fb]")
for i in range(len(yodas)):
    myplt.print_xs(names[i], yodas[i]["/MC_HEJ_HJET_ADD2/cutflow"].points()[2])
print("xs VBF [fb]")
for i in range(len(yodas)):
    xsec_vbf[i] = myplt.print_xs(names[i], yodas[i]["/MC_HEJ_HJET_ADD2/cutflow"].points()[4])



def plot_hist(analysis_name, hist_name, ymax_in, ymin_in, ratioymin_in, ratioymax_in, xlabel_in, ylabel_in, info_text_in, logy_in=True, addtext = '', addtag = ''):
    histo_path = analysis_name +hist_name
    histos = [h[histo_path] for h in yodas]
    histos_central = [h[histo_path] for h in yodas_central]
    histo_ref = histos[index_ref]
    fig, (main_axis, ratio_axis) = myplt.init_fig(histos, ratio=True,
    xlabel=xlabel_in,
    ylabel=ylabel_in,
    ratioylabel=ratiolabel)

    for i in range(len(histos)):
        myplt.plot_histo_givencentral(main_axis,ratio_axis[0],histos[i], histos_central[i], href=histo_ref, **hist_options[i])

    leg=main_axis.legend(names_long,fontsize=12,loc=1)
    frame = leg.get_frame()
    frame.set_fill(False)
    frame.set_linewidth(0)

    if addtext != '':
        myplt.add_text(main_axis,0.04,0.16,info_text_in + [addtext])
    else:
        myplt.add_text(main_axis,0.04,0.16,info_text_in)
    myplt.setup_axes(histos, main_axis, ratio_axis, logy=logy_in,ymax=ymax_in,ymin=ymin_in,
    ratioymin=ratioymin_in,ratioymax=ratioymax_in, ratioplotlogy=True)

    fig.set_tight_layout(True)
    plt.savefig(output_dir+hist_name+ addtag + '.pdf', bbox_inches='tight')
    plt.savefig(output_dir+hist_name+ addtag + '.png', bbox_inches='tight', scale=0.1)
    plt.close()
    print()

def plot_hist_xsec(analysis_name, fig_name, hist_name_1, hist_name_2, ymax_in, ymin_in, ratioymax_in, ratioymin_in,  xlabel1_in, xlabel2_in, ylabel_in, info_text_in, logy_in=True, addtext = '', addtag =''):

    histo_path_1 = analysis_name+hist_name_1
    histos_1 = [myplt.rebin(h[histo_path_1],2) for h in yodas]
    histos_central_1 = [myplt.rebin(h[histo_path_1],2) for h in yodas_central]
    histo_ref_1 = histos_1[index_ref]

    histo_path_2 = analysis_name+hist_name_2
    histos_2 = [myplt.rebin(h[histo_path_2],2) for h in yodas]
    histos_central_2 = [myplt.rebin(h[histo_path_2],2) for h in yodas_central]
    histo_ref_2 = histos_2[index_ref]

    #print(len(histos_1), len(histos_2))

    hist_options_1 = [{"color":"blue", "linestyle":"-", "errorbars":False},
                {"color":"red", "linestyle":"-", "errorbars":False}, {"color":"green", "linestyle":"-", "errorbars":False}, {"color":"blue", "linestyle":"-", "errorbars":False}]

    hist_options_2 = [{"color":"blue", "linestyle":":", "errorbars":False},
                {"color":"red", "linestyle":":", "errorbars":False}, {"color":"green", "linestyle":"-", "errorbars":False}, {"color":"blue", "linestyle":":", "errorbars":False}]

    
    fig, (main_axis, ratio_axis) = myplt.init_fig(histos_1, ratio=True,
    ylabel='$\\sigma(\mathrm{ggF})$',
    xlabel='$\\sigma($' + names_long[index_ref] + '$) [\\mathrm{fb}]$',
    ratioylabel=ratiolabel)

    #for i in range(len(histos_1)):
    myplt.plot_xsec_histo_givencentral(main_axis,ratio_axis[0],histos_1[1],histos_central_1[0], href=histo_ref_1, **hist_options_1[0])
    myplt.plot_xsec_histo_givencentral(main_axis,ratio_axis[0],histos_1[1],histos_central_1[1], href=histo_ref_1, **hist_options_1[1])

    #for i in range(len(histos_2)):
    myplt.plot_xsec_histo_givencentral(main_axis,ratio_axis[0],histos_2[0],histos_central_2[0], href=histo_ref_2, **hist_options_2[0])
    myplt.plot_xsec_histo_givencentral(main_axis,ratio_axis[0],histos_2[1],histos_central_2[1], href=histo_ref_2, **hist_options_2[1])


    #leg=main_axis.legend(names_long,fontsize=14,loc=1)
    main_axis.plot(np.NaN, np.NaN, color=hist_options_1[1]["color"], alpha=1, label=names_long[1], lw=1.8)
    main_axis.plot(np.NaN, np.NaN, color=hist_options_1[0]["color"], alpha=1, label=names_long[0], lw=1.8)
    main_axis.plot(np.NaN, np.NaN, color="black", alpha=1, label=xlabel1_in, lw=1.8, ls="-")
    main_axis.plot(np.NaN, np.NaN, color="black", alpha=1, label=xlabel2_in, lw=1.8, ls=":")
    leg = main_axis.legend(fontsize=8, loc=1)

    frame = leg.get_frame()
    frame.set_fill(False)
    frame.set_linewidth(0)

    if addtext != '':
        myplt.add_text(main_axis,0.04,0.56,info_text_in + [addtext])
    else:
        myplt.add_text(main_axis,0.04,0.56,info_text_in)
   
    myplt.setup_axes(histos_1, main_axis, ratio_axis, logy=False, ymin=ymin_in, ymax=ymax_in,
                         ratioymin=ratioymin_in,ratioymax=ratioymax_in, xmin=min(histo_ref_2.yVals()), xmax=max(histo_ref_2.yVals()))

    fig.set_tight_layout(True)

    plt.savefig(output_dir+fig_name+ addtag + '.pdf', bbox_inches='tight')
    plt.savefig(output_dir+fig_name+ addtag + '.png', bbox_inches='tight', scale=0.1)
    plt.close()
    

### MC_HEJ_HJET_ADD2:
## jet12_mass_dijet_A (invariant mass of the two tagging jets)
## jet12_mass_VBF_A (invariant mass of the two tagging jets, leading jet selection)
## H_y_VBF_A (rapidity of the Higgs boson , VBF cuts, leading jet selection)
## H_pT_VBF_A (pT of the Higgs boson , VBF cuts, leading jet selection)
## H_y_VBF_B (rapidity of the Higgs boson , VBF cuts, F-B jet selection)
## H_pT_VBF_B (pT of the Higgs boson , VBF cuts, F-B jet selection)
## drcmin_VBF_A (cut around minimum Delta R with tagging jets, for only jets inbetween tagging jets, leading jet selection)
## drcmin_VBF_B (cut around minimum Delta R with tagging jets, for only jets inbetween tagging jets, FB jet selection)
## drcmin_VBF_C (cut around minimum Delta R with tagging jets, for only jets inbetween tagging jets, leading jet selection + three jets or more)
## drcmin_VBF_D (cut around minimum Delta R with tagging jets, for only jets inbetween tagging jets, FB jet selection + three jets or more)
## yc_VBF_A (central jet veto, VBF cuts, leading jet selection)
## yc_VBF_B (central jet veto, VBF cuts, F-B jet selection)
## yc_VBF_C (central jet veto, VBF cuts, leading jet selection + three jets or more)
## yc_VBF_D (central jet veto, VBF cuts, leading jet selection + three jets or more)


### MC_HJETS_LH15:
## HT_all (scalar sum of jet transverse momenta and Higgs transverse mas)
## deltay_H_jj (rapidity separation of Higgs boson and leading dijet system)
## deltay_jj (rapidity separation of leading jets)
## dijet_mass (Higgs plus two leading jet mass)
## jet1_pT_incl (transverse momentum of leading jet Njet >= 1)
## jet1_y (rapidity of leading jet)
## jet2_pT_incl (transverse momentum of subleading jet Njet >=2)
## jjdy_dy (rapidity separation of most forward and most backward jets)
## jjpT_dy (rapidity separation of leading jets)
## xs_central_jet_veto_VBF (jet cross section with y_dist = min | y_jet - (y_fw+y_bw)/2 | < y^cut_dist, FB jet selection) 
## xs_central_jet_veto_VBF2  (jet cross section with y_dist = min | y_jet - (y_fw+y_bw)/2 | < y^cut_dist, leading jet selection) 
ANALYSIS = '/MC_HEJ_HJET_ADD2/'


plot_hist_xsec(ANALYSIS, 'cutxsecmin_VBF_A', 'yc_VBF_A', 'drc_VBF_A', 0.4, 0.0, 0.2, 0.0,  '$y_c$', '$\Delta R_c^\mathrm{min}$', '$\\sigma(\mathrm{ggF})$', info_text, addtext='VBF cuts, leading jet selection', logy_in=False)
plot_hist_xsec(ANALYSIS, 'cutxsecmin_VBF_B', 'yc_VBF_B', 'drc_VBF_B', 0.4, 0.0, 0.2, 0.0,  '$y_c$', '$\Delta R_c^\mathrm{min}$', '$\\sigma(\mathrm{ggF})$', info_text, addtext='VBF cuts, forward-backward jet selection', logy_in=False)
plot_hist_xsec(ANALYSIS, 'cutxsecmin_VBF_C', 'yc_VBF_C', 'drc_VBF_C', 0.125, 0.0, 1.0, 0.0,  '$y_c$', '$\Delta R_c^\mathrm{min}$', '$\\sigma(\mathrm{ggF})$', info_text, addtext='VBF cuts + at least three jets, leading jet selection', logy_in=False)
plot_hist_xsec(ANALYSIS, 'cutxsecmin_VBF_D', 'yc_VBF_D', 'drc_VBF_D', 0.125, 0.0, 1.0, 0.0,  '$y_c$', '$\Delta R_c^\mathrm{min}$', '$\\sigma(\mathrm{ggF})$', info_text, addtext='VBF cuts + at least three jets, forward-backward jet selection', logy_in=False)

plot_hist(ANALYSIS, 'jet12_mass_dijet_A', 1E-1, 5E-5, 0.0, 6.0, '$m_{j_1 j_2} [\\mathrm{GeV}]$', '$\\mathrm{d}\\sigma/\\mathrm{d} m_{j_1 j_2} [\\mathrm{fb}/\\mathrm{GeV}]$', info_text)
plot_hist(ANALYSIS, 'jet12_mass_VBF_A', 2E-3, 1E-5, 0.5, 4.0, '$m_{j_1 j_2} [\\mathrm{GeV}]$', '$\\mathrm{d}\\sigma/\\mathrm{d} m_{j_1 j_2} [\\mathrm{fb}/\\mathrm{GeV}]$', info_text, addtext='VBF cuts, leading jet selection')
plot_hist(ANALYSIS, 'H_y_VBF_A', 0.7, 0.0, 0.0, 1.1, '$y_H$', '$\\mathrm{d}\\sigma/\\mathrm{d} y_H [\\mathrm{fb}]$', info_text, addtext='VBF cuts, leading jet selection', logy_in=False)
plot_hist(ANALYSIS, 'H_pT_VBF_A', 0.02, 1E-6, 0.0, 2.1, '$p_{T,H} [\\mathrm{GeV}]$', '$\\mathrm{d}\\sigma/\\mathrm{d} p_{T,H} [\\mathrm{fb}/\\mathrm{GeV}]$', info_text, addtext='VBF cuts, leading jet selection')
plot_hist(ANALYSIS, 'H_y_VBF_B', 0.7, 0.0, 0.0, 2.0, '$y_H$', '$\\mathrm{d}\\sigma/\\mathrm{d} y_H [\\mathrm{fb}]$', info_text, addtext='VBF cuts, forward-backward jet selection', logy_in=False)
plot_hist(ANALYSIS, 'H_pT_VBF_B', 0.02, 1E-6, 0.0, 4.0, '$p_{T,H} [\\mathrm{GeV}]$', '$\\mathrm{d}\\sigma/\\mathrm{d} p_{T,H} [\\mathrm{fb}/\\mathrm{GeV}]$', info_text, addtext='VBF cuts, forward-backward jet selection')
plot_hist(ANALYSIS, 'drcmin_VBF_A', 2.0, 0.0, 0.0, 1.5, '$\\Delta R_{c,\mathrm{min}}$ (inside jets)', '$\\sigma (\\Delta R_{c,\mathrm{min}}) [\\mathrm{fb}]$', info_text, addtext='VBF cuts, leading jet selection', logy_in=False)
plot_hist(ANALYSIS, 'drcmin_VBF_B', 2.0, 0.0, 0.0, 1.5, '$\\Delta R_{c,\mathrm{min}}$ (inside jets)', '$\\sigma (\\Delta R_{c,\mathrm{min}}) [\\mathrm{fb}]$', info_text, addtext='VBF cuts, forward-backward jet selection', logy_in=False)
plot_hist(ANALYSIS, 'drcmin_VBF_C', 0.25, 0.0, 0.0, 2.0, '$\\Delta R_{c,\mathrm{min}}$ (inside jets)', '$\\sigma (\\Delta R_{c,\mathrm{min}}) [\\mathrm{fb}]$', info_text, addtext='VBF cuts + at least three jets, leading jet selection', logy_in=False)
plot_hist(ANALYSIS, 'drcmin_VBF_D',  0.25, 0.0, 0.0, 2.0, '$\\Delta R_{c,\mathrm{min}}$ (inside jets)', '$\\sigma (\\Delta R_{c,\mathrm{min}}) [\\mathrm{fb}]$', info_text, addtext='VBF cuts + at least three jets, forward-backward jet selection', logy_in=False)

plot_hist(ANALYSIS, 'drc_VBF_A', 2.0, 0.0, 0.0, 0.7, '$\\Delta R_{c,\mathrm{min}}$ (all jets)', '$\\sigma (\\Delta R_{c,\mathrm{min}}) [\\mathrm{fb}]$', info_text, addtext='VBF cuts, leading jet selection', logy_in=False)
plot_hist(ANALYSIS, 'drc_VBF_B', 2.0, 0.0, 0.0, 0.7, '$\\Delta R_{c,\mathrm{min}}$ (all jets)', '$\\sigma (\\Delta R_{c,\mathrm{min}}) [\\mathrm{fb}]$', info_text, addtext='VBF cuts, forward-backward jet selection', logy_in=False)
plot_hist(ANALYSIS, 'drc_VBF_C', 0.25, 0.0, 0.0, 2.0, '$\\Delta R_{c,\mathrm{min}}$ (all jets)', '$\\sigma (\\Delta R_{c,\mathrm{min}}) [\\mathrm{fb}]$', info_text, addtext='VBF cuts + at least three jets, leading jet selection', logy_in=False)
plot_hist(ANALYSIS, 'drc_VBF_D',  0.25, 0.0, 0.0, 2.0, '$\\Delta R_{c,\mathrm{min}}$ (all jets)', '$\\sigma (\\Delta R_{c,\mathrm{min}}) [\\mathrm{fb}]$', info_text, addtext='VBF cuts + at least three jets, forward-backward jet selection', logy_in=False)


plot_hist(ANALYSIS, 'yc_VBF_A', 2.0, 0.0, 0.0, 0.7, '$y_c$', '$\\sigma (y_c) [\\mathrm{fb}]$', info_text, addtext='VBF cuts, leading jet selection', logy_in=False)
plot_hist(ANALYSIS, 'yc_VBF_B', 2.0, 0.0, 0.0, 0.7, '$y_c$', '$\\sigma (y_c) [\\mathrm{fb}]$', info_text, addtext='VBF cuts, forward-backward jet selection', logy_in=False)
plot_hist(ANALYSIS, 'yc_VBF_C', 0.25, 0.0, 0.0, 2, '$y_c$', '$\\sigma (y_c) [\\mathrm{fb}]$', info_text, addtext='VBF cuts + at least three jets, leading jet selection', logy_in=False)
plot_hist(ANALYSIS, 'yc_VBF_D', 0.25, 0.0, 0.0, 2, '$y_c$', '$\\sigma (y_c) [\\mathrm{fb}]$', info_text, addtext='VBF cuts + at least three jets, forward-backward jet selection', logy_in=False)




ANALYSIS = '/MC_HJETS_LH15/'
plot_hist(ANALYSIS, 'HT_all', 1E-1, 1E-4, 0.0, 4.0, '$H_T [\\mathrm{GeV}]$', '$\\mathrm{d}\\sigma/\\mathrm{d} H_T [\\mathrm{fb}/\\mathrm{GeV}]$', info_text, addtext='scalar sum of jet transverse momenta and Higgs transverse mass')
plot_hist(ANALYSIS, 'deltay_H_jj', 10., 1E-4, 0.0, 4.0, '$\\Delta y(H,jj)$', '$\\mathrm{d}\\sigma/\\mathrm{d} \\Delta y(H,jj) [\\mathrm{fb}]$', info_text, addtext='rapidity separation of Higgs boson and leading dijet system')
plot_hist(ANALYSIS, 'deltay_jj', 100., 1E-6, 0.0, 4.0, '$\\Delta y(j,j)$', '$\\mathrm{d}\\sigma/\\mathrm{d} \\Delta y(j,j) [\\mathrm{fb}]$', info_text, addtext='rapidity separation of leading jets')
plot_hist(ANALYSIS, 'H_dijet_mass', 7E-2, 1E-4, 0.0, 4.0, '$m_{Hj_1j_2} [\\mathrm{GeV}]$', '$\\mathrm{d}\\sigma/\\mathrm{d} m_{Hj_1 j_2} [\\mathrm{fb}/\\mathrm{GeV}]$', info_text, addtext='Higgs plus two leading jet mass')
plot_hist(ANALYSIS, 'jet1_pT_incl', 3E-1, 1E-3, 0.0, 3.0, '$p_{T,j_1} [\\mathrm{GeV}]$', '$\\mathrm{d}\\sigma/\\mathrm{d} p_{T,j_1} [\\mathrm{fb}/\\mathrm{GeV}]$', info_text, addtext='transverse momentum of leading jet, $N_\\mathrm{jets} \geq 1$')
plot_hist(ANALYSIS, 'jet1_y', 10., 1E-2, 0.0, 3, '$y_{j_1}$', '$\\mathrm{d}\\sigma/\\mathrm{d} y_{j_1} [\\mathrm{fb}]$', info_text, addtext='rapidity of leading jet, $N_\\mathrm{jets} \geq 1$')
plot_hist(ANALYSIS, 'jet2_pT_incl', 1.0, 1E-3, 0.0, 3, '$p_{T,j_2} [\\mathrm{GeV}]$', '$\\mathrm{d}\\sigma/\\mathrm{d} p_{T,j_2} [\\mathrm{fb}/\\mathrm{GeV}]$', info_text, addtext='transverse momentum of sub-leading jet, $N_\\mathrm{jets} \geq 2$')
plot_hist(ANALYSIS, 'jjdy_dy', 10., 1E-5, 0.0, 4.5, '$\\Delta y(j_F,j_B)$', '$\\mathrm{d}\\sigma/\\mathrm{d} \\Delta y(j_F,j_B)[\\mathrm{fb}]$', info_text, addtext='rapidity separation of most forward and most backward jets')
plot_hist(ANALYSIS, 'jjpT_dy', 10., 1E-5, 0.0, 4.5, '$\\Delta y(j_1,j_2)$', '$\\mathrm{d}\\sigma/\\mathrm{d} \\Delta y(j_1,j_2)[\\mathrm{fb}]$', info_text, addtext='rapidity separation of leading jets')
plot_hist(ANALYSIS, 'xs_central_jet_veto_VBF2', 2.5, 0.0, 0.8, 5.0, r'$y_\mathrm{dist}^\mathrm{cut}$', r'$\sigma(y_\mathrm{dist})<y_\mathrm{dist}^\mathrm{cut}$ [fb]', info_text, addtext=r'VBF cuts, leading jet selection, $y_\mathrm{dist}=\min\limits_\mathrm{jets}\left|y_\mathrm{jet}-\frac{y_\mathrm{fw}+y_\mathrm{bw}}{2}\right|<y_\mathrm{dist}^\mathrm{cut}$', logy_in=False)
plot_hist(ANALYSIS, 'xs_central_jet_veto_VBF', 2.0, 0.0, 0.8, 5.0,r'$y_\mathrm{dist}^\mathrm{cut}$', r'$\sigma(y_\mathrm{dist})<y_\mathrm{dist}^\mathrm{cut}$ [fb]', info_text, addtext=r'VBF cuts, forward-backward jet selection, $y_\mathrm{dist}=\min\limits_\mathrm{jets}\left|y_\mathrm{jet}-\frac{y_\mathrm{fw}+y_\mathrm{bw}}{2}\right|<y_\mathrm{dist}^\mathrm{cut}$', logy_in=False)



###################
# WRITE HTML FILE #
###################

# get the files in the directory:
#print glob.glob(outputdir + "/*.png")

tag = 'index'

# search for files and plot them:
#pngfiles = glob.glob(output_dir + "/*.png")
#htmltags = [t.replace('.png','') for t in pngfiles]

# or create manually:
plots = ['HT_all', 'H_dijet_mass', 'deltay_H_jj', 'deltay_jj', 'jet12_mass_dijet_A', 'H_y_VBF_A', 'H_pT_VBF_A', 'H_y_VBF_B', 'H_pT_VBF_B', 'jet1_pT_incl', 'jet1_y', 'jet2_pT_incl', 'jjdy_dy', 'jjpT_dy', 'yc_VBF_A', 'yc_VBF_B', 'yc_VBF_C', 'yc_VBF_D', 'drcmin_VBF_A', 'drcmin_VBF_B', 'drcmin_VBF_C', 'drcmin_VBF_D', 'drc_VBF_A', 'drc_VBF_B', 'drc_VBF_C', 'drc_VBF_D', 'cutxsecmin_VBF_A', 'cutxsecmin_VBF_B', 'cutxsecmin_VBF_C', 'cutxsecmin_VBF_D']
htmltags = []
for p in range(len(plots)):
    htmltags.append(output_dir + plots[p])
    
print(htmltags)

write_html(output_dir,tag, htmltags, 'h+jets')


exit()

print(info_text)
    
## HT_all

hist_name = 'HT_all'

histo_path = '/MC_HJETS_LH15/'+hist_name
histos = [h[histo_path] for h in yodas]
histos_central = [h[histo_path] for h in yodas_central]
histo_ref = histos[index_ref]
fig, (main_axis, ratio_axis) = myplt.init_fig(histos, ratio=True,
  xlabel='$H_T [\\mathrm{GeV}]$',
  ylabel='$\\mathrm{d}\\sigma/\\mathrm{d} H_T [\\mathrm{fb}/\\mathrm{GeV}]$',
  ratioylabel=ratiolabel)

for i in range(len(histos)):
  myplt.plot_histo_givencentral(main_axis,ratio_axis[0],histos[i], histos_central[i], href=histo_ref, **hist_options[i])

leg=main_axis.legend(names_long,fontsize=12,loc=1)
frame = leg.get_frame()
frame.set_fill(False)
frame.set_linewidth(0)
myplt.add_text(main_axis,0.04,0.14,info_text)
myplt.setup_axes(histos, main_axis, ratio_axis, logy=True,ymax=7e-02,ymin=2.5e-04,
  ratioymin=0.2,ratioymax=4., ratioplotlogy=True, title=hist_name.replace('_','-'))

fig.set_tight_layout(True)
plt.savefig(output_dir+hist_name+'.pdf', bbox_inches='tight')
plt.savefig(output_dir+hist_name+'.png', bbox_inches='tight', scale=0.1)
plt.close()
print()

## H_dijet_mass

hist_name = 'H_dijet_mass'

histo_path = '/MC_HJETS_LH15/'+hist_name
histos = [h[histo_path] for h in yodas]
histos_central = [h[histo_path] for h in yodas_central]
histo_ref = histos[index_ref]
fig, (main_axis, ratio_axis) = myplt.init_fig(histos, ratio=True,
  xlabel='$m_{jj}  [\\mathrm{GeV}]$',
  ylabel='$\\mathrm{d}\\sigma/\\mathrm{d} m_{jj} [\\mathrm{fb}/\\mathrm{GeV}]$',
  ratioylabel=ratiolabel)

for i in range(len(histos)):
  myplt.plot_histo_givencentral(main_axis,ratio_axis[0],histos[i], histos_central[i], href=histo_ref, **hist_options[i])

leg=main_axis.legend(names_long,fontsize=12,loc=1)
frame = leg.get_frame()
frame.set_fill(False)
frame.set_linewidth(0)
myplt.add_text(main_axis,0.04,0.14,info_text)
myplt.setup_axes(histos, main_axis, ratio_axis, logy=True,ymax=5e-2,ymin=2.5e-05,
  ratioymin=0.2,ratioymax=4., ratioplotlogy=True)

fig.set_tight_layout(True)
plt.savefig(output_dir+hist_name+'.pdf', bbox_inches='tight')
plt.savefig(output_dir+hist_name+'.png', bbox_inches='tight', scale=0.1)
plt.close()
print()

## jjfb_dy

hist_name = 'jjfb_dy'

histo_path = '/MC_HJETS_LH15/'+hist_name
histos = [h[histo_path] for h in yodas]
histos_central = [h[histo_path] for h in yodas_central]
histo_ref = histos[index_ref]
fig, (main_axis, ratio_axis) = myplt.init_fig(histos, ratio=True,
  xlabel='$\Delta y_\mathrm{fb}  [\\mathrm{GeV}]$',
  ylabel='$\\mathrm{d}\\sigma/\\mathrm{d} \Delta y_\mathrm{fb} [\\mathrm{fb}/\\mathrm{GeV}]$',
  ratioylabel=ratiolabel)

for i in range(len(histos)):
  myplt.plot_histo_givencentral(main_axis,ratio_axis[0],histos[i], histos_central[i], href=histo_ref, **hist_options[i])

leg=main_axis.legend(names_long,fontsize=12,loc=1)
frame = leg.get_frame()
frame.set_fill(False)
frame.set_linewidth(0)
myplt.add_text(main_axis,0.04,0.14,info_text)
myplt.setup_axes(histos, main_axis, ratio_axis, logy=True,ymax=10.,ymin=2.5e-02,
  ratioymin=0.2,ratioymax=4., ratioplotlogy=True)

fig.set_tight_layout(True)
plt.savefig(output_dir+hist_name+'.pdf', bbox_inches='tight')
plt.savefig(output_dir+hist_name+'.png', bbox_inches='tight', scale=0.1)
plt.close()
print()

## jjpT_dy

hist_name = 'jjpT_dy'

histo_path = '/MC_HJETS_LH15/'+hist_name
histos = [h[histo_path] for h in yodas]
histos_central = [h[histo_path] for h in yodas_central]
histo_ref = histos[index_ref]
fig, (main_axis, ratio_axis) = myplt.init_fig(histos, ratio=True,
  xlabel='$\Delta y_\mathrm{12}  [\\mathrm{GeV}]$',
  ylabel='$\\mathrm{d}\\sigma/\\mathrm{d} \Delta y_\mathrm{12} [\\mathrm{fb}/\\mathrm{GeV}]$',
  ratioylabel=ratiolabel)

for i in range(len(histos)):
  myplt.plot_histo_givencentral(main_axis,ratio_axis[0],histos[i], histos_central[i], href=histo_ref, **hist_options[i])

leg=main_axis.legend(names_long,fontsize=12,loc=1)
frame = leg.get_frame()
frame.set_fill(False)
frame.set_linewidth(0)
myplt.add_text(main_axis,0.04,0.14,info_text)
myplt.setup_axes(histos, main_axis, ratio_axis, logy=True,ymax=10.,ymin=2.5e-03,
  ratioymin=0.2,ratioymax=4., ratioplotlogy=True)

fig.set_tight_layout(True)
plt.savefig(output_dir+hist_name+'.pdf', bbox_inches='tight')
plt.savefig(output_dir+hist_name+'.png', bbox_inches='tight', scale=0.1)
plt.close()
print()


## Higgs pt

hist_name = 'H_pT_dijet_A'

histo_path = '/MC_HEJ_HJET_ADD2/'+hist_name
histos = [h[histo_path] for h in yodas]
histos_central = [h[histo_path] for h in yodas_central]
histo_ref = histos[index_ref]
fig, (main_axis, ratio_axis) = myplt.init_fig(histos, ratio=True,
  xlabel='$p_{H\\perp} [\\mathrm{GeV}]$',
  ylabel='$\\mathrm{d}\\sigma/\\mathrm{d}p_{H\\perp} [\\mathrm{fb}/\\mathrm{GeV}]$',
  ratioylabel=ratiolabel)

for i in range(len(histos)):
  myplt.plot_histo_givencentral(main_axis,ratio_axis[0],histos[i], histos_central[i], href=histo_ref, **hist_options[i])

leg=main_axis.legend(names_long,fontsize=12,loc=1)
frame = leg.get_frame()
frame.set_fill(False)
frame.set_linewidth(0)
myplt.add_text(main_axis,0.04,0.14,info_text)
myplt.setup_axes(histos, main_axis, ratio_axis, logy=True,ymax=1E-1,ymin=2.5e-05,
  ratioymin=0.2,ratioymax=4., ratioplotlogy=True)

fig.set_tight_layout(True)
plt.savefig(output_dir+hist_name+'.pdf', bbox_inches='tight')
plt.savefig(output_dir+hist_name+'.png', bbox_inches='tight', scale=0.1)
plt.close()
print()

## m_12

hist_name = 'jet12_mass_dijet_A'

histo_path = '/MC_HEJ_HJET_ADD2/'+hist_name
histos = [h[histo_path] for h in yodas]
histos_central = [h[histo_path] for h in yodas_central]
histo_ref = histos[index_ref]
fig, (main_axis, ratio_axis) = myplt.init_fig(histos, ratio=True,
  xlabel='$m_{12} [\\mathrm{GeV}]$',
  ylabel='$\\mathrm{d}\\sigma/\\mathrm{d}m_{12} [\\mathrm{fb}/\\mathrm{GeV}]$',
  ratioylabel=ratiolabel)

for i in range(len(histos)):
    myplt.plot_histo_givencentral(main_axis,ratio_axis[0],histos[i], histos_central[i],href=histo_ref,
      **hist_options[i])

leg=main_axis.legend(names_long,fontsize=12,loc=1)
frame = leg.get_frame()
frame.set_fill(False)
frame.set_linewidth(0)
myplt.add_text(main_axis,0.04,0.14,info_text)
myplt.setup_axes(histos, main_axis, ratio_axis, logy=True, ymin=9e-05, ymax=4e-02,
  ratioymin=0.4,ratioymax=4.)

fig.set_tight_layout(True)
plt.savefig(output_dir+hist_name+'.pdf', bbox_inches='tight')
plt.savefig(output_dir+hist_name+'.png', bbox_inches='tight', scale=0.1)
plt.close()

